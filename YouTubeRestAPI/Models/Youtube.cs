﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using System.ComponentModel;
using Newtonsoft.Json.Converters;

namespace YouTubeRestAPI.Models
{
    public class PostJob
    {
        [Required, Range(1, 99999999, ErrorMessage = "inos_story_id is required and between 1..99999999")]
        public long inos_story_id { get; set; }
        [Required, StringLength(255, ErrorMessage = "inos_story_name is required and max 255 characters")]
        public string inos_story_name { get; set; }
        [Required, Range(1, 99999999, ErrorMessage = "inos_task_id is required and between 1..99999999")]
        public long inos_task_id { get; set; }
        [Required, StringLength(255, ErrorMessage = "inos_task_name is required and max 255 characters")]
        public string inos_task_name { get; set; }
        [CustomFilters.Uri, Required]
        public string job_url { get; set; }
        [Required, RegularExpression(@"^[a-zA-Z0-9_#-<>+=\[\]\{\}]+$", ErrorMessage = "inos_asset_name contains invalid characters")]
        public string inos_asset_name { get; set; }
        [Required, MaxLength(32)]
        public string ingest_service_job_id { get; set; }
        public long job_id { get; set; }
        public string job_render_location { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Status? job_status { get; set; }
        [Range(0, 100)]
        public int? job_progress { get; set; }
        [DataType(DataType.Date)]
        public DateTime job_creation_date { get; set; }
        [DataType(DataType.Date)]
        public DateTime? job_start_date { get; set; }
        [DataType(DataType.Date)]
        public DateTime? job_end_date { get; set; }
        public string job_error_description { get; set; }
        public MovieMetadata movie_metadata { get; set; }
        [Range(typeof(TimeSpan), "00:00:00", "23:59:59"), Display(Name = "movie_in")]
        public TimeSpan? movie_in { get; set; }
        [Range(typeof(TimeSpan), "00:00:00", "23:59:59"), CustomFilters.GreaterThan("movie_in"), Display(Name ="movie_out")]
        public TimeSpan? movie_out { get; set; }
        public int? worker_number { get; set; }
        [Required, JsonConverter(typeof(StringEnumConverter))]
        public iNOSEnvironment? inos_environment { get; set; }

    }

    public class PutJob
    {
        [Required, Range(1, 99999999, ErrorMessage = "job_id is required and between 1..99999999")]
        public long job_id { get; set; }
        [StringLength(255, ErrorMessage = "inos_task_name is required and max 255 characters")]
        public string job_render_location { get; set; }
        [EnumDataType(typeof(Status))]
        public Status? job_status { get; set; }
        [Range(0,100)]
        public int? job_progress { get; set; }
        [DataType(DataType.Date)]
        public DateTime? job_start_date { get; set; }
        [DataType(DataType.Date)]
        public DateTime? job_end_date { get; set; }
        public string job_error_description { get; set; }
        public MovieMetadata movie_metadata { get; set; }
        public int? worker_number { get; set; }
        [EnumDataType(typeof(Movieformat))]
        public Movieformat? movie_format { get; set; }
        [EnumDataType(typeof(JobType))]
        public JobType? job_type { get; set; }
    }

    public class UrlFilter
    {
        [EnumDataType(typeof(JobType))]
        public string type_name { get; set; }
        public int type_id { get; set; }
        public string url_filter { get; set; }
    }


    public class MovieMetadata
    {
        public int? fps { get; set; }
        public int duration { get; set; }
        public string title { get; set; }
        public string vcodec { get; set; }
        public string acodec { get; set; }
        public int? abitrate { get; set; }
        public string aformat { get; set; }
        public string vformat { get; set; }
        public string hxw { get; set; }
        public string license { get; set; }
        public string description { get; set; }
        public string uploader { get; set; }
        public string uploader_id { get; set; }
        public double? start_time { get; set; }
    }

    public enum Movieformat
    {
        Unknown,
        DASH,
        Single,
        Live
    }

    public enum Status
    {
        Created,
        Waiting,
        Metadata,
        Downloading,
        Running,
        Finished,
        Failed
    }

    public enum JobType
    {
        YoutubeMovie,
        YoutubeList,
        YoutubeLive,
        TwitterMovie,
        FacebookMovie,
        Instagram,
        Vimeo,
        Dailymotion
    }

    public enum iNOSEnvironment
    {
        Development,
        Testing,
        Training,
        Production
    }
}