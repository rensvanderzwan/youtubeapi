﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace YouTubeRestAPI.Models
{

    public class GetWorkers
    {
        public GetWorker workers;
    }

    public class GetWorker
    {
        public int worker_id { get; set; }
        public WorkerStatus? worker_status { get; set; }
        public WorkerActivity? worker_activity { get; set; }
    }

    public class PutWorker
    {
        [Required]
        public int worker_id { get; set; }
        [EnumDataType(typeof(WorkerStatus))]
        public WorkerStatus? worker_status { get; set; }
        [EnumDataType(typeof(WorkerActivity))]
        public WorkerActivity? worker_activity { get; set; }
    }

    public enum WorkerStatus
    {
        Enabled,
        Disabled
    }

    public enum WorkerActivity
    {
        Waiting,
        Running
    }

}