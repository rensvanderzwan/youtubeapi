﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace YouTubeRestAPI.Controllers
{
    public class JobListController : Controller
    {
        // GET: JobList
        public ActionResult Index(string status, int page = 1)
        {
            ViewBag.status = status;
            ViewBag.page = page;
            int limit = 40;
            int offset = (page - 1) * limit;

            List<Models.PostJob> jobList = new YouTubeDBIO().getJobs(offset, limit, status);
            if (jobList == null)
            {
                jobList = new List<Models.PostJob>();
                jobList.Add(new Models.PostJob() { job_error_description = "Error retrieving Jobs" });
            }

            Response.AddHeader("Refresh", "15");

            return View(jobList);
        }

        // GET: JobList/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: JobList/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: JobList/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: JobList/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: JobList/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: JobList/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: JobList/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
