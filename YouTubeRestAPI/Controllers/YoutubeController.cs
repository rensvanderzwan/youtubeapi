﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using YouTubeRestAPI.Filters;
using YouTubeRestAPI.Models;

namespace YouTubeRestAPI.Controllers
{
    [BasicAuthentication]
    public class YoutubeController : ApiController
    {
        [Route("Youtube/GetJobs/{page:int:min(1)}/{limit:min(1):int=20}")]
        [HttpGet]
        public HttpResponseMessage GetJobs(int page, int limit)
        {            
            int offset = (page - 1) * limit;
            List<PostJob> jobList = new YouTubeDBIO().getJobs(offset, limit);
            if (jobList == null)
            {
                var message = string.Format("Jobs not found");
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, jobList);
        }


        [Route("Youtube/GetJobs/{*uri}")]
        [HttpGet]
        public HttpResponseMessage GetJobs()
        {
            // whe're here because the url did not match the route
            return Request.CreateResponse(HttpStatusCode.NotFound, "Bad Route , should look like: api/Youtube/GetJobs/<page:int(1..)>[/<limit:int(1..)>]");
        }

        [Route("Youtube/GetNewJob/{type:job_type}")]
        [HttpGet]
        public HttpResponseMessage GetNewJobs(JobType type)
        {
            List<PostJob> job = new YouTubeDBIO().getNewJob(type);
            if ( job == null)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Internal Server error");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, job);
            }
        }

        [Route("Youtube/GetNewJob/{somevalue}")]
        [HttpGet]
        public HttpResponseMessage GetNewJobs(object somevalue)
        {
            // whe're here because the url did not match the route
            string[] JobTypeNames = Enum.GetNames(typeof(JobType));
            string JobTypeOptions = string.Join("|", JobTypeNames);
            return Request.CreateResponse(HttpStatusCode.NotFound, $"Bad Route , should look like: api/Youtube/GetNewJob/<{JobTypeOptions}>");
        }

        [Route("Youtube/GetNewJob/")]
        [HttpGet]
        public HttpResponseMessage GetNewJobs()
        {
            List<PostJob> job = new YouTubeDBIO().getNewJob(null);
            if (job == null)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Internal Server error");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, job);
            }
        }


        [Route("Youtube/GetStatus/{status:status_type}/{page:int:min(1)=1}/{limit:min(1):int=20}")]
        [HttpGet]
        public HttpResponseMessage GetStatus(Status status, int page, int limit)
        {
                int offset = (page - 1) * limit;
                List<PostJob> jobList = new YouTubeDBIO().getJobs(offset, limit, status.ToString());
                if (jobList == null)
                {
                    var message = string.Format("Jobs not found");
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, message);
                }
                return Request.CreateResponse(HttpStatusCode.OK, jobList);
        }

        [Route("Youtube/GetStatus/{*uri}")]
        [HttpGet]
        public HttpResponseMessage GetStatus()
        {
            // whe're here because the url did not match the route
            string[] StatusNames = Enum.GetNames(typeof(Status));
            string StatusOptions = string.Join("|",StatusNames);
            return Request.CreateResponse(HttpStatusCode.NotFound, $"Bad Route , should look like: api/Youtube/GetStatus/<{StatusOptions}>[/<page:int>[/<limit/int>]]");
        }


        [Route("Youtube/GetJob/{id:long}")]
        [HttpGet]
        public HttpResponseMessage GetJob(long id)
        {
            List<PostJob> job = new YouTubeDBIO().getJobs(0,1,null,id);
            if (job == null || job.Count() != 1)
            {
                var message = string.Format("Job with id = {0} not found", id);
                return Request.CreateResponse(HttpStatusCode.NotFound, message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, job[0]);
        }

        [Route("Youtube/AliveCheck")]
        [HttpGet]
        public HttpResponseMessage AliveCheck()
        {
            List<PostJob> job = new YouTubeDBIO().getJobs(0, 1, null, null);
            if (job == null)
            {
                
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "AliveCheck Failed");
            }
            return Request.CreateResponse(HttpStatusCode.OK, "We're still alive and well");
        }


        [Route("Youtube/GetJob/{*uri}")]
        [HttpGet]
        public HttpResponseMessage GetJob()
        {
            // whe're here because the url did not match the route
            return Request.CreateResponse(HttpStatusCode.NotFound, "Bad Route , should look like: api/Youtube/GetJob/<job_id:int>");
        }

        [Route("Youtube")]
        [HttpPost]
        [CustomFilters.CheckModelForNull]
        [CustomFilters.ValidateModelState]
        public HttpResponseMessage Post([FromBody]PostJob value)
        {
            long id = new YouTubeDBIO().saveJob(value, out string ErrorMessage);

            // make a correct location if the requested uri has trailing slashes or not
            string relPath;
            if (Request.RequestUri.AbsolutePath.EndsWith("/"))
            {
                relPath = "GetJob/" + id.ToString();
            }
            else
            {
                relPath = "YouTube/GetJob/" + id.ToString();
            }

            if ( id != 0)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                response.Headers.Location = new Uri(Request.RequestUri, string.Format("{0}", relPath));
                return response;
            }
            else
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Headers.Location = new Uri(Request.RequestUri, $"Error saving data to the Database {ErrorMessage}");
                return response;
            }
        }

        [Route("Youtube/Workers")]
        [CustomFilters.CheckModelForNull]
        [CustomFilters.ValidateModelState]
        [HttpPut]
        public HttpResponseMessage PutWorker( [FromBody]PutWorker value)
        {
          //  value.worker_id = id;
            long WorkerUpdated = new YouTubeDBIO().updateWorker(value, out string ErrorMessage);
            if (ErrorMessage == "")
            {
                return Request.CreateResponse(HttpStatusCode.OK, $"{WorkerUpdated} worker updated");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, $"Error updating database: {ErrorMessage}");
            }
        }

        [Route("Youtube/GetWorkers")]
        [HttpGet]
        public HttpResponseMessage GetWorkers()
        {
            List<GetWorker> job = new YouTubeDBIO().getworkers();
            if (job == null)
            {
                var message ="Workers not found";
                return Request.CreateResponse(HttpStatusCode.NotFound, message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, job);
        }


        [Route("Youtube")]
        [HttpPut]
        [CustomFilters.CheckModelForNull]
        [CustomFilters.ValidateModelState]
        public HttpResponseMessage Put( [FromBody]PutJob value)
        {
         //   value.job_id = id;
            long Recordupdated = new YouTubeDBIO().updateJob(value, out string ErrorMessage);
            if (ErrorMessage == "")
            {
                return Request.CreateResponse(HttpStatusCode.OK, $"{Recordupdated} records updated");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, $"Error updating database: {ErrorMessage}");
            }
        }


        [Route("Youtube/GetJobTypes")]
        [HttpGet]
        public HttpResponseMessage GetJobTypes()
        {
            List<UrlFilter> job = new YouTubeDBIO().getjobtypes();
            if (job == null)
            {
                var message = "Job types not found";
                return Request.CreateResponse(HttpStatusCode.NotFound, message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, job);
        }



        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }


    }
}
