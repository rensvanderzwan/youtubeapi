﻿// see: https://www.strathweb.com/2012/10/clean-up-your-web-api-controllers-with-model-validation-and-null-check-filters/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace YouTubeRestAPI
{
    public class CustomFilters
    {
        [AttributeUsage(AttributeTargets.Method, Inherited = true)]
        public class CheckModelForNullAttribute : ActionFilterAttribute
        {
            private readonly Func<Dictionary<string, object>, bool> _validate;

            public CheckModelForNullAttribute() : this(arguments =>
            arguments.ContainsValue(null))
            { }

            public CheckModelForNullAttribute(Func<Dictionary<string, object>, bool> checkCondition)
            {
                _validate = checkCondition;
            }

            public override void OnActionExecuting(HttpActionContext actionContext)
            {
                if (_validate(actionContext.ActionArguments))
                {
                    actionContext.Response = actionContext.Request.CreateErrorResponse(
                     HttpStatusCode.BadRequest, "The argument cannot be null");
                }
            }
        }


        public class UriAttribute : ValidationAttribute
        {
            // allow only valid url's with http or https scheme.
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                bool valid = Uri.TryCreate(Convert.ToString(value), UriKind.Absolute, out Uri uri)
                    && (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps);

                if (!valid)
                {
                    return new ValidationResult(ErrorMessage);
                }
                return ValidationResult.Success;
            }
        }

        public class GreaterThanAttribute : ValidationAttribute
        {

            public GreaterThanAttribute(string otherProperty)
                : base("{0} must be greater than {1}")
            {
                OtherProperty = otherProperty;
            }

            public string OtherProperty { get; set; }

            public string FormatErrorMessage(string name, string otherName)
            {
                return string.Format(ErrorMessageString, name, otherName);
            }

            protected override ValidationResult IsValid(object firstValue, ValidationContext validationContext)
            {
                IComparable firstComparable = firstValue as IComparable;
                var secondComparable = GetSecondComparable(validationContext);

                if (firstComparable != null && secondComparable != null)
                {
                    if (firstComparable.CompareTo(secondComparable) < 1)
                    {
                        object obj = validationContext.ObjectInstance;
                        var thing = obj.GetType().GetProperty(OtherProperty);
                        var displayName = (DisplayAttribute)Attribute.GetCustomAttribute(thing, typeof(DisplayAttribute));

                        return new ValidationResult(
                            FormatErrorMessage(validationContext.DisplayName, displayName.GetName()));
                    }
                }

                return ValidationResult.Success;
            }

            protected IComparable GetSecondComparable(
                ValidationContext validationContext)
            {
                var propertyInfo = validationContext
                                      .ObjectType
                                      .GetProperty(OtherProperty);
                if (propertyInfo != null)
                {
                    var secondValue = propertyInfo.GetValue(
                        validationContext.ObjectInstance, null);
                    return secondValue as IComparable;
                }
                return null;
            }
        }


        public class ValidateModelStateAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(HttpActionContext actionContext)
            {
                if (!actionContext.ModelState.IsValid)
                {

                    foreach(var eror in actionContext.ModelState.Values)
                    {
                        var x = eror.Errors;
                        foreach( var y in x)
                        {
                            var z = y.Exception?.Message;
                        }
                    }
                    var ErrorMessage = string.Join(" | ", actionContext.ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                    var ExceptionMessage = string.Join(" | ", actionContext.ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.Exception?.Message));

                    sLogger.WriteLog(sLogger.MsgType.ERROR, "", $" ValidationError: { ErrorMessage} | {ExceptionMessage}");

                    actionContext.Response = actionContext.Request.CreateErrorResponse(
                    HttpStatusCode.BadRequest, actionContext.ModelState);
                }
            }
        }
    }
}