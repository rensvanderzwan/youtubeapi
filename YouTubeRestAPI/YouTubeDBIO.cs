﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using YouTubeRestAPI.Models;

namespace YouTubeRestAPI
{
    public class YouTubeDBIO
    {
        private readonly MySqlConnection _conn;

        public YouTubeDBIO()
        {
            string myConnString = "server=localhost;uid=itbo;pwd=diwemww11;database=youtube";
            try
            {
                _conn = new MySqlConnection();
                _conn.ConnectionString = myConnString;
                _conn.Open();
            }
            catch (MySqlException ex)
            {
                sLogger.WriteLog(sLogger.MsgType.ERROR, "", "Unable to Logon: " +  ex.Message);
            }
        }


        public long updateJob(PutJob jobToUpdate, out string ErrorMessage)
        {

            ErrorMessage = "";
            try
            {
                string query = @"UPDATE jobs SET 
                                job_render_location = COALESCE(@job_render_location, job_render_location),
                                job_error_description = IF(ISnull(job_error_description),@job_error_description,CONCAT_WS('\r\n',job_error_description,@job_error_description)),
                                job_progress = COALESCE(@job_progress, job_progress),
                                job_start_date = COALESCE(@job_start_date, job_start_date),
                                job_end_date = COALESCE(@job_end_date, job_end_date),
                                job_status = COALESCE((SELECT status_id FROM job_status WHERE status_name = @job_status), job_status),
                                movie_format = COALESCE((SELECT format_id FROM movie_format WHERE format_name = @movie_format), movie_format),
                                job_type = COALESCE((SELECT type_id FROM job_type WHERE type_name = @job_type), job_type),
                                movie_metadata = COALESCE(@movie_metadata, movie_metadata),
                                worker_number = COALESCE(@worker_number, worker_number)
                                WHERE job_id = @job_id";

                MySqlCommand cmd = new MySqlCommand(query, _conn);

                string data = null;
                if (jobToUpdate.movie_metadata == null)
                {
                    data = null;
                }
                else
                {
                    data = JsonConvert.SerializeObject(jobToUpdate.movie_metadata);
                }

                cmd.Parameters.AddWithValue("@job_id", jobToUpdate.job_id);
                cmd.Parameters.AddWithValue("@job_error_description", jobToUpdate.job_error_description);
                cmd.Parameters.AddWithValue("@job_render_location", jobToUpdate.job_render_location);
                cmd.Parameters.AddWithValue("@job_progress", jobToUpdate.job_progress);
                cmd.Parameters.AddWithValue("@job_start_date", jobToUpdate.job_start_date);
                cmd.Parameters.AddWithValue("@job_end_date", jobToUpdate.job_end_date);
                cmd.Parameters.AddWithValue("@job_status", jobToUpdate.job_status.ToString());
                cmd.Parameters.AddWithValue("@movie_metadata", data);
                cmd.Parameters.AddWithValue("@worker_number", jobToUpdate.worker_number);
                cmd.Parameters.AddWithValue("@movie_format", jobToUpdate.movie_format.ToString());
                cmd.Parameters.AddWithValue("@job_type", jobToUpdate.job_type.ToString());

                int updated = cmd.ExecuteNonQuery();
                return updated;
            }
            catch (MySqlException ex)
            {
                ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                sLogger.WriteLog(sLogger.MsgType.ERROR, "", $"Job_id: {jobToUpdate.job_id} {ErrorMessage} job: {JsonConvert.SerializeObject(jobToUpdate)}");
                return 0;
            }
            finally
            {
                _conn.Close();
            }
        }

        public long saveJob(PostJob jobToSave, out string ErrorMessage)
        {
            ErrorMessage = "";
            try
            {
                string query = @"INSERT INTO jobs (     job_url,
                                                        inos_story_id,
                                                        inos_story_name,
                                                        inos_task_id,
                                                        inos_task_name,
                                                        inos_asset_name,
                                                        ingest_service_job_id,
                                                        movie_in,
                                                        movie_out,
                                                        inos_environment
                               ) VALUES ( 
                                                        @job_url,
                                                        @inos_story_id,
                                                        @inos_story_name,
                                                        @inos_task_id,
                                                        @inos_task_name,
                                                        @inos_asset_name,
                                                        @ingest_service_job_id,
                                                        @movie_in,
                                                        @movie_out,
                                                        @inos_environment
                                )";

                MySqlCommand cmd = new MySqlCommand(query, _conn);

                cmd.Parameters.AddWithValue("@job_url", jobToSave.job_url);
                cmd.Parameters.AddWithValue("@inos_story_id", jobToSave.inos_story_id);
                cmd.Parameters.AddWithValue("@inos_story_name", jobToSave.inos_story_name);
                cmd.Parameters.AddWithValue("@inos_task_id", jobToSave.inos_task_id);
                cmd.Parameters.AddWithValue("@inos_task_name", jobToSave.inos_task_name);
                cmd.Parameters.AddWithValue("@inos_asset_name", jobToSave.inos_asset_name);
                cmd.Parameters.AddWithValue("@ingest_service_job_id", jobToSave.ingest_service_job_id);
                cmd.Parameters.AddWithValue("@movie_in", jobToSave.movie_in);
                cmd.Parameters.AddWithValue("@movie_out", jobToSave.movie_out);
                cmd.Parameters.AddWithValue("@inos_environment", jobToSave.inos_environment);
                cmd.ExecuteNonQuery();

                sLogger.WriteLog(sLogger.MsgType.INFO, "", $"Job_id: {cmd.LastInsertedId} added to the Database: {JsonConvert.SerializeObject(jobToSave)}");
                return cmd.LastInsertedId;
            }
            catch (MySqlException ex)
            {
                sLogger.WriteLog(sLogger.MsgType.ERROR, "", ex.Message);
                ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                return 0;
            }
            finally
            {
                _conn.Close();
            }
        }

        public List<PostJob> getNewJob(JobType? type)
        {
            int[] JobTypeInList;
            if ( type == null)
            {
                JobTypeInList =  Enum.GetValues(typeof(JobType)).Cast<int>().ToArray();
            }
            else
            {
                JobTypeInList = new int[] { (int) type };
            }


            Guid id;
            try
            {
                id = Guid.NewGuid();
                string query = $@"UPDATE jobs SET guid = @id WHERE job_status=0 AND guid is NULL LIMIT 1";

                MySqlCommand UpdateCmd = new MySqlCommand(query, _conn);
                UpdateCmd.Parameters.AddWithValue("@id", id);
                UpdateCmd.Parameters.AddWithValue("@job_type", string.Join(",", JobTypeInList));
                int updated = UpdateCmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                sLogger.WriteLog(sLogger.MsgType.ERROR, "", $"Unable to set guid: {ErrorMessage}");
                _conn.Close();
                return null;
            }

            List<PostJob> jobList = new List<PostJob>();

            MySqlDataReader mySQlReader = null;
            string sqlString = @"SELECT 
                                    * 
                                 FROM 
                                    jobs
	                             LEFT JOIN job_type ON ( jobs.job_type = job_type.type_id )
	                             LEFT JOIN job_status ON ( jobs.job_status = job_status.status_id )
                                 WHERE guid = @id ";

            MySqlCommand selectCmd = new MySqlCommand(sqlString, _conn);
            selectCmd.Parameters.AddWithValue("@id", id);

            try
            {
                mySQlReader = selectCmd.ExecuteReader();
                while (mySQlReader.Read())
                {
                    PostJob j = new PostJob();
                    j.job_id = Convert.ToInt64(mySQlReader.GetString("job_id"));
                    j.job_url = mySQlReader["job_url"].ToString();
                    j.inos_story_id = Convert.ToInt64(mySQlReader["inos_story_id"]);
                    j.inos_story_name = mySQlReader["inos_story_name"].ToString();
                    j.inos_task_id = Convert.ToInt64(mySQlReader["inos_task_id"]);
                    j.inos_task_name = mySQlReader["inos_task_name"].ToString();
                    j.ingest_service_job_id = mySQlReader["ingest_service_job_id"].ToString();
                    j.job_status = (Status) mySQlReader["job_status"];
                    j.job_creation_date = Convert.ToDateTime(mySQlReader["job_creation_date"]);
                    j.inos_asset_name = mySQlReader["inos_asset_name"].ToString();
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("movie_in")))
                        j.movie_in = TimeSpan.Parse(mySQlReader["movie_in"].ToString());
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("movie_out")))
                        j.movie_out = TimeSpan.Parse(mySQlReader["movie_out"].ToString());
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("inos_environment")))
                        j.inos_environment = (iNOSEnvironment) mySQlReader["inos_environment"];

                    jobList.Add(j);
                }
                mySQlReader.Close();
                return jobList;
            }
            catch (Exception ex)
            {
                sLogger.WriteLog(sLogger.MsgType.ERROR, "", $" GetJobs: {ex.Message}");
                return null;
            }
            finally
            {
                _conn.Close();
            }
        }

        public List<PostJob> getJobs(int offset, int limit, string status = null, long? job_id = null, string type_name = null)
        {
            List<PostJob> jobList = new List<PostJob>();

            MySqlDataReader mySQlReader = null;
            string sqlString = $@"SELECT
	                                 * 
                                 FROM
	                                 jobs
	                                 LEFT JOIN job_type ON ( jobs.job_type = job_type.type_id )
	                                 LEFT JOIN job_status ON ( jobs.job_status = job_status.status_id )
                                     WHERE (status_name = @status OR @status IS NULL)
                                     And (job_id = @job_id or @job_id IS NULL)
                                     And (job_type = @job_type or @job_type IS NULL)
                                     ORDER BY job_creation_date DESC
                                     LIMIT @limit OFFSET @offset";

            MySqlCommand cmd = new MySqlCommand(sqlString, _conn);
            cmd.Parameters.AddWithValue("@limit", limit);
            cmd.Parameters.AddWithValue("@offset", offset);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@job_id", job_id);
            cmd.Parameters.AddWithValue("@job_type", type_name);

            try
            {
                mySQlReader = cmd.ExecuteReader();
                while (mySQlReader.Read())
                {
                    PostJob j = new PostJob();
                    j.job_id = Convert.ToInt64(mySQlReader.GetString("job_id"));
                    j.job_url = mySQlReader["job_url"].ToString();
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("job_render_location")))
                        j.job_render_location = mySQlReader["job_render_location"].ToString();
                    j.inos_story_id = Convert.ToInt64(mySQlReader["inos_story_id"]);
                    j.inos_story_name = mySQlReader["inos_story_name"].ToString();
                    j.inos_task_id = Convert.ToInt64(mySQlReader["inos_task_id"]);
                    j.inos_task_name = mySQlReader["inos_task_name"].ToString();
                    j.ingest_service_job_id = mySQlReader["ingest_service_job_id"].ToString();
                    j.job_status = (Status) mySQlReader["job_status"];
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("job_progress")))
                        j.job_progress = Convert.ToInt32(mySQlReader["job_progress"]);
                    j.job_creation_date = Convert.ToDateTime(mySQlReader["job_creation_date"]);
                    j.inos_asset_name = mySQlReader["inos_asset_name"].ToString();
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("job_start_date")))
                        j.job_start_date = Convert.ToDateTime(mySQlReader["job_start_date"]);
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("job_end_date")))
                        j.job_end_date = Convert.ToDateTime(mySQlReader["job_end_date"]);
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("job_error_description")))
                        j.job_error_description = mySQlReader["job_error_description"].ToString();
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("movie_metadata")))
                        j.movie_metadata = JsonConvert.DeserializeObject<MovieMetadata> ( mySQlReader["movie_metadata"].ToString());
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("worker_number")))
                        j.worker_number = Convert.ToInt32(mySQlReader["worker_number"]);
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("movie_in")))
                        j.movie_in = TimeSpan.Parse(mySQlReader["movie_in"].ToString());
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("movie_out")))
                        j.movie_out = TimeSpan.Parse(mySQlReader["movie_out"].ToString());
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("inos_environment")))
                        j.inos_environment = (iNOSEnvironment) mySQlReader["inos_environment"];

                    jobList.Add(j);
                }
                mySQlReader.Close();
                return jobList;
            }
            catch (Exception ex)
            {
                sLogger.WriteLog(sLogger.MsgType.ERROR, "", $" GetJobs: {ex.Message}");
                return null;
            }
            finally
            {
                _conn.Close();
            }
        }

        public List<GetWorker> getworkers()
        {
            List<GetWorker> WorkerList = new List<GetWorker>();

            MySqlDataReader mySQlReader = null;
            string sqlString = $@"SELECT * FROM `workers`
                                  LEFT JOIN worker_status ON ( workers.worker_status = worker_status.status_id)
                                  LEFT JOIN worker_activity ON ( workers.worker_activity = worker_activity.activity_id)
                                  ORDER BY worker_id;";

            MySqlCommand cmd = new MySqlCommand(sqlString, _conn);

            try
            {
                mySQlReader = cmd.ExecuteReader();
                while (mySQlReader.Read())
                {
                    GetWorker worker = new GetWorker();
                    worker.worker_id = Convert.ToInt32(mySQlReader.GetString("worker_id"));
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("activity_name")))
                        worker.worker_activity = (WorkerActivity) Convert.ToInt32(mySQlReader["activity_id"]);
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("status_name")))
                        worker.worker_status = (WorkerStatus) Convert.ToInt32(mySQlReader["status_id"]);
                    WorkerList.Add(worker);
                }
                mySQlReader.Close();
                return WorkerList;
            }
            catch (Exception ex)
            {
                sLogger.WriteLog(sLogger.MsgType.ERROR, "", $" GetJobs: {ex.Message}");
                return null;
            }
            finally
            {
                _conn.Close();
            }
        }


        public long updateWorker(PutWorker workerToUpdate, out string ErrorMessage)
        {
            ErrorMessage = "";
            try
            {
                string query = @"UPDATE workers SET 
                                worker_status = COALESCE((SELECT status_id FROM worker_status WHERE status_name = @worker_status), worker_status),
                                worker_activity = COALESCE((SELECT activity_id FROM worker_activity WHERE activity_name = @worker_activity), worker_activity)
                                WHERE worker_id = @worker_id";

                MySqlCommand cmd = new MySqlCommand(query, _conn);
                cmd.Parameters.AddWithValue("@worker_id", workerToUpdate.worker_id);
                cmd.Parameters.AddWithValue("@worker_status", workerToUpdate.worker_status.ToString());
                cmd.Parameters.AddWithValue("@worker_activity", workerToUpdate.worker_activity.ToString());
                int updated = cmd.ExecuteNonQuery();
                return updated;
            }
            catch (MySqlException ex)
            {
                sLogger.WriteLog(sLogger.MsgType.ERROR, "", ex.Message);
                ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                return 0;
            }
            finally
            {
                _conn.Close();
            }
        }

        public List<UrlFilter> getjobtypes()
        {
            List<UrlFilter> JobTypeList = new List<UrlFilter>();

            MySqlDataReader mySQlReader;
            string sqlString = $@"select * from url_filter LEFT JOIN job_type on ( url_filter.job_type = job_type.type_id)";

            MySqlCommand cmd = new MySqlCommand(sqlString, _conn);

            try
            {
                mySQlReader = cmd.ExecuteReader();
                while (mySQlReader.Read())
                {
                    UrlFilter jobtype = new UrlFilter();
                    jobtype.type_id = Convert.ToInt32(mySQlReader.GetString("type_id"));
                    if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("type_name")))
                        jobtype.type_name = mySQlReader["type_name"].ToString();
                    jobtype.url_filter =  mySQlReader["url_filter"].ToString();
                    JobTypeList.Add(jobtype);
                }
                mySQlReader.Close();
                return JobTypeList;
            }
            catch (Exception ex)
            {
                sLogger.WriteLog(sLogger.MsgType.ERROR, "", $" GetJobsTypes: {ex.Message}");
                return null;
            }
            finally
            {
                _conn.Close();
            }
        }
    }
}