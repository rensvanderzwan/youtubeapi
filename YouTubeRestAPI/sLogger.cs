﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace YouTubeRestAPI
{
    public class RabbitLogMsg
    {
        public string msgType { get; set; }
        public string location { get; set; }
        public string processName { get; set; }
        public string refId { get; set; }
        public string msg { get; set; }
        public string msgDetail { get; set; }
        public long timestamp { get; set; }
    }


    public static class sLogger
    {
        public enum MsgType
        {
            INFO,
            WARN,
            ERROR,
            DEBUG
        }

        private static ReaderWriterLockSlim _readWriteLock = new ReaderWriterLockSlim();
        public static string _logPath { get; set; }
        public static string _rabbitLocation { get; set; }
        public static string _rabbitProcessName { get; set; }

        public static void Start(string LogPath)
        {
            _logPath = LogPath;
            RotateLog();
            WriteConsoleAndLog(sLogger.MsgType.INFO, "", "############### Start ###############");
            var timer = new Timer(e => RotateLog(), null, TimeSpan.Zero, TimeSpan.FromMinutes(60));
        }

        private static void RotateLog()
        {
            if (File.Exists(_logPath))
            {
                FileInfo f = new FileInfo(_logPath);
                if (f.Length > 5 * 1024 * 1024)
                {
                    string ArchiveName = _logPath + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".log";
                    File.Delete(ArchiveName); // Delete the existing file if exists
                    File.Move(_logPath, ArchiveName);
                    WriteLog(MsgType.INFO, "--", $"Log Rotation: {ArchiveName}");
                }
            }
        }


        public static void WriteLog(MsgType LogType, string refID, string Message)
        {
            _readWriteLock.EnterWriteLock();        //enable lock
            Directory.CreateDirectory(Path.GetDirectoryName(_logPath));
            try
            {
                using (StreamWriter w = File.AppendText(_logPath))
                {
                    w.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} {refID.PadLeft(6, ' ')} {LogType.ToString()}: {Message}");
                    w.Close();
                }
            }
            finally
            {
                _readWriteLock.ExitWriteLock();         //release lock
            }
        }

        public static void WriteConsoleAndLog(MsgType LogType, string refID, string Message)
        {
            _readWriteLock.EnterWriteLock();        //enable lock
            Directory.CreateDirectory(Path.GetDirectoryName(_logPath));
            try
            {
                using (StreamWriter w = File.AppendText(_logPath))
                {
                    w.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} {refID.PadLeft(6, ' ')} {LogType.ToString()}: {Message}");
                  //  Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} {refID.PadLeft(6, ' ')} {Message}");
                }
            }
            finally
            {
                _readWriteLock.ExitWriteLock();         //release lock
            }
        }

        //public static void WriteConsoleAndLogAndBus(MsgType LogType, string refID, string Message, string DetailMessage = "")
        //{

        //    WriteConsoleAndLog(LogType, refID, Message);

        //    RabbitLogMsg msg = new RabbitLogMsg();
        //    msg.msgType = LogType.ToString();
        //    msg.location = _rabbitLocation;
        //    msg.processName = _rabbitProcessName;
        //    msg.refId = refID;
        //    msg.msg = Message;
        //    msg.msgDetail = DetailMessage;
        //    msg.timestamp = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds * 1000;

        //    string message = JsonConvert.SerializeObject(msg);
        //    var body = Encoding.UTF8.GetBytes(message);

        //    try
        //    {
        //        sRabbitConnector.Channel.BasicPublish(exchange: sRabbitConnector._LogExchange,
        //                             routingKey: sRabbitConnector._LogRoutingKey,
        //                             mandatory: false,
        //                             basicProperties: sRabbitConnector.CreateBasicProperties("logging"),
        //                             body: body);
        //    }
        //    catch (Exception)
        //    {
        //        // No need to reconnect here, this is allready taken care of in the Main loop.
        //    }
        //}
    }
}