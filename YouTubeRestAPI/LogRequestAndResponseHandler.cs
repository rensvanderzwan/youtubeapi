﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Net;

namespace YouTubeRestAPI
{
    public class LogRequestAndResponseHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // log request body
            string requestBody = await request.Content.ReadAsStringAsync();
            if( request.Method == HttpMethod.Post)
            {
                // Log the raw request

            }

            // let other handlers process the request
            var response = await base.SendAsync(request, cancellationToken);

            HttpStatusCode statusCode = response.StatusCode;

            string responseBody = "";
            if (response.Content != null)
            {
                responseBody = await response.Content.ReadAsStringAsync();
                if (statusCode != HttpStatusCode.Created && request.Method == HttpMethod.Post)
                {
                    sLogger.WriteLog(sLogger.MsgType.ERROR, "", "Response:" + responseBody);
                    sLogger.WriteLog(sLogger.MsgType.ERROR, "", "PostJob:" + requestBody);
                }
            }



            //if ( result.StatusCode != System.Net.HttpStatusCode.Created && request.Method == HttpMethod.Post)
            //{
            //    var responseBody = await result.Content.ReadAsStringAsync();
            //    sLogger.WriteLog(sLogger.MsgType.ERROR, "", "PostJob:" + requestBody);
            //}

            //if (result.Content != null && request.Method == HttpMethod.Post)
            //{
            //    // once response body is ready, log it
            //    var responseBody = await result.Content.ReadAsStringAsync();
            //    sLogger.WriteLog(sLogger.MsgType.ERROR, "", "PostJob:" + requestBody);
            //}

            return response;
        }
    }
}