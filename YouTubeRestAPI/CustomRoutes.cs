﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Routing;
using YouTubeRestAPI.Models;

namespace YouTubeRestAPI
{
    public class CustomRoutes
    {
    }

    public class JobTypesConstraint : IHttpRouteConstraint
    {
        public bool Match(HttpRequestMessage request, IHttpRoute route, string parameterName,
            IDictionary<string, object> values, HttpRouteDirection routeDirection)
        {
            object value;
            if (values.TryGetValue(parameterName, out value))
            {
                JobType jobType;
                if (Enum.TryParse<JobType>(value.ToString(),true ,out jobType))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }

    public class StatusConstraint : IHttpRouteConstraint
    {
        public bool Match(HttpRequestMessage request, IHttpRoute route, string parameterName,
            IDictionary<string, object> values, HttpRouteDirection routeDirection)
        {
            object value;
            if (values.TryGetValue(parameterName, out value) && value != null)
            {
                if (Enum.TryParse<Status>(value.ToString(), true, out Status status))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }


}